/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 *
 * @author Francisca Gómez
 */
public class Tiposervicio implements ValidarTipoServicio {
    //CONSTRUCTORES - SET - GET

    int id_tipo_servicio_tecnico;
    String descripcion_tipo_servicio_tecnico;
    String estado_tipo_servicio;

    public Tiposervicio() {
    }

    public Tiposervicio(int id_tipo_servicio_tecnico, String descripcion_tipo_servicio_tecnico, String estado_tipo_servicio) {
        this.id_tipo_servicio_tecnico = id_tipo_servicio_tecnico;
        this.descripcion_tipo_servicio_tecnico = descripcion_tipo_servicio_tecnico;
        this.estado_tipo_servicio = estado_tipo_servicio;
    }
    
    

    public int getId_tipo_servicio_tecnico() {
        return id_tipo_servicio_tecnico;
    }

    public void setId_tipo_servicio_tecnico(int id_tipo_servicio_tecnico) {
        this.id_tipo_servicio_tecnico = id_tipo_servicio_tecnico;
    }

    public String getDescripcion_tipo_servicio_tecnico() {
        return descripcion_tipo_servicio_tecnico;
    }

    public void setDescripcion_tipo_servicio_tecnico(String descripcion_tipo_servicio_tecnico) {
        this.descripcion_tipo_servicio_tecnico = descripcion_tipo_servicio_tecnico;
    }

    public String getEstado_tipo_servicio() {
        return estado_tipo_servicio;
    }

    public void setEstado_tipo_servicio(String estado_tipo_servicio) {
        this.estado_tipo_servicio = estado_tipo_servicio;
    }

    //FUNCIONES REGISTRAR - BUSCAR - MODIFICAR - CAMBIAR ESTADO 
    Connection con;
    Conexion cn = new Conexion();
    PreparedStatement ps;
    ResultSet rs;

    @Override
    public int registrarTiposervicio(Tiposervicio ts) {
        int r = 0;
        String sql = "INSERT INTO public.tipo_servicio_tecnico(descripcion_tipo_servicio_tecnico, estado_tipo_servicio)\n"
                + "	VALUES (?,?)";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, ts.getDescripcion_tipo_servicio_tecnico());
            ps.setString(2, ts.getEstado_tipo_servicio());
            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;
                ts.setDescripcion_tipo_servicio_tecnico(rs.getString("descripcion_tipo_servicio_tecnico"));
                ts.setEstado_tipo_servicio(rs.getString("estado_tipo_servicio"));
            }
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            return 0;
        }
    }

    public HashMap seleccionarTiposervicio() {
        HashMap<String, String> dropts_ts = new HashMap();
        try {
            ConectaBD conn = new ConectaBD();
            String sql = "SELECT id_tipo_servicio_tecnico as id_ts,descripcion_tipo_servicio_tecnico\n"
                    + "	FROM public.tipo_servicio_tecnico\n"
                    + " WHERE estado_tipo_servicio = 'activo';";
            conn.abrir();
            rs = conn.con.createStatement().executeQuery(sql);
            while (rs.next()) {
                dropts_ts.put(rs.getString("id_ts"), rs.getString("descripcion_tipo_servicio_tecnico"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            System.out.println("No anda tu cagda");
        }
        return dropts_ts;
    }

    public Tiposervicio buscarTipoServicio(String buscartxt) {
        int r = 0;
        Tiposervicio ts = new Tiposervicio();
        String sql = "SELECT id_tipo_servicio_tecnico, descripcion_tipo_servicio_tecnico\n"
                + "FROM tipo_servicio_tecnico\n"
                + "WHERE descripcion_tipo_servicio_tecnico = ?\n"
                + "AND estado_tipo_servicio = 'activo';";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, buscartxt);

            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;
                ts.setDescripcion_tipo_servicio_tecnico(rs.getString("descripcion_tipo_servicio_tecnico"));
                ts.setId_tipo_servicio_tecnico(rs.getInt("id_tipo_servicio_tecnico"));
            }
        } catch (Exception e) {
            System.out.println("hola");
        } finally {
            return ts;
        }
    }

    public int modificarTipoServicio(Tiposervicio ts) {
        int r = 0;
        String sql = "UPDATE public.tipo_servicio_tecnico\n"
                + "SET descripcion_tipo_servicio_tecnico=?, estado_tipo_servicio=?\n"
                + "WHERE id_tipo_servicio_tecnico=?;";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, ts.getDescripcion_tipo_servicio_tecnico());
            ps.setString(2, ts.getEstado_tipo_servicio());
            ps.setInt(3, ts.getId_tipo_servicio_tecnico());
            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;
                ts.setDescripcion_tipo_servicio_tecnico(rs.getString("descripcion_departamento"));
                //ts.setEstado_tipo_servicio(rs.getString("estado_tipo_servicio"));
            }
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            return 0;
        }
    }
}
