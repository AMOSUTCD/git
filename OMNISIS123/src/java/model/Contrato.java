/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 *
 * @author Alvaro
 */
public class Contrato implements ValidarContrato {

    int id_contrato, id_cliente;
    String cliente_contrato, fiscal_contrato, codigo_contrato, validez_contrato, fecha_inicio, fecha_fin, tipo_contrato, estado_contrato, nom_usu, descripcion_cliente;

    public Contrato() {
    }

    public Contrato(int id_contrato, int id_cliente, String cliente_contrato, String fiscal_contrato, String codigo_contrato, String validez_contrato, String fecha_inicio, String fecha_fin, String tipo_contrato, String estado_contrato, String nom_usu, String descripcion_cliente) {
        this.id_contrato = id_contrato;
        this.id_cliente = id_cliente;
        this.cliente_contrato = cliente_contrato;
        this.fiscal_contrato = fiscal_contrato;
        this.codigo_contrato = codigo_contrato;
        this.validez_contrato = validez_contrato;
        this.fecha_inicio = fecha_inicio;
        this.fecha_fin = fecha_fin;
        this.tipo_contrato = tipo_contrato;
        this.estado_contrato = estado_contrato;
        this.nom_usu = nom_usu;
        this.descripcion_cliente = descripcion_cliente;
    }

    public int getId_contrato() {
        return id_contrato;
    }

    public void setId_contrato(int id_contrato) {
        this.id_contrato = id_contrato;
    }

    public int getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getCliente_contrato() {
        return cliente_contrato;
    }

    public void setCliente_contrato(String cliente_contrato) {
        this.cliente_contrato = cliente_contrato;
    }

    public String getFiscal_contrato() {
        return fiscal_contrato;
    }

    public void setFiscal_contrato(String fiscal_contrato) {
        this.fiscal_contrato = fiscal_contrato;
    }

    public String getCodigo_contrato() {
        return codigo_contrato;
    }

    public void setCodigo_contrato(String codigo_contrato) {
        this.codigo_contrato = codigo_contrato;
    }

    public String getValidez_contrato() {
        return validez_contrato;
    }

    public void setValidez_contrato(String validez_contrato) {
        this.validez_contrato = validez_contrato;
    }

    public String getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public String getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(String fecha_fin) {
        this.fecha_fin = fecha_fin;
    }

    public String getTipo_contrato() {
        return tipo_contrato;
    }

    public void setTipo_contrato(String tipo_contrato) {
        this.tipo_contrato = tipo_contrato;
    }

    public String getEstado_contrato() {
        return estado_contrato;
    }

    public void setEstado_contrato(String estado_contrato) {
        this.estado_contrato = estado_contrato;
    }

    public String getNom_usu() {
        return nom_usu;
    }

    public void setNom_usu(String nom_usu) {
        this.nom_usu = nom_usu;
    }

    public String getDescripcion_cliente() {
        return descripcion_cliente;
    }

    public void setDescripcion_cliente(String descripcion_cliente) {
        this.descripcion_cliente = descripcion_cliente;
    }

    

    Connection con;
    Conexion cn = new Conexion();
    PreparedStatement ps;
    ResultSet rs;

    @Override
    public int registrarContrato(Contrato cont) {
        int r = 0;
        String sql = "INSERT INTO public.contrato(\n"
                + "cliente_contrato, fiscal_contrato, codigo_contrato, validez_contrato, fecha_inicio_contrato, fecha_final_contrato, \n"
                + "tipo_contrato, estado_contrato, nombre_usuario_modificacion, fecha_modificacion, id_cliente)\n"
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, (SELECT current_date), ?);";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, cont.getCliente_contrato());
            ps.setString(2, cont.getFiscal_contrato());
            ps.setString(3, cont.getCodigo_contrato());
            ps.setString(4, cont.getValidez_contrato());
            ps.setString(5, cont.getFecha_inicio());
            ps.setString(6, cont.getFecha_fin());
            ps.setString(7, cont.getTipo_contrato());
            ps.setString(8, cont.getEstado_contrato());
            ps.setString(9, cont.getNom_usu());
            ps.setInt(10, cont.getId_cliente());

            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;
                //Variables.id = Integer.parseInt(rs.getString("id_usuario"));
                cont.setCliente_contrato(rs.getString("cliente_contrato"));
                cont.setFiscal_contrato(rs.getString("fiscal_contrato"));
                cont.setCodigo_contrato(rs.getString("codigo_contrato"));
                cont.setValidez_contrato(rs.getString("validez_contrato"));
                cont.setFecha_inicio(rs.getString("fecha_inicio_contrato"));
                cont.setFecha_fin(rs.getString("fecha_final_contrato"));
                cont.setTipo_contrato(rs.getString("tipo_contrato"));
                cont.setEstado_contrato(rs.getString("estado_contrato"));
                cont.setNom_usu(rs.getString("nombre_usuario_modificacion"));
                cont.setId_cliente(rs.getInt("id_cliente"));
            }
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }

        } catch (Exception e) {
            return 0;
        }
    }

    public HashMap seleccionarContrato() {
        HashMap<String, String> drop_cont = new HashMap();
        try {
            ConectaBD conn = new ConectaBD();
            String sql = "SELECT id_contrato as id_cont,codigo_contrato\n"
                    + "                    FROM public.contrato\n"  //agregar \n" a todos los que no funciona el WHERE
                    + "                    WHERE estado_contrato = 'activo';";
            conn.abrir();
            rs = conn.con.createStatement().executeQuery(sql);
            while (rs.next()) {
                drop_cont.put(rs.getString("id_cont"), rs.getString("codigo_contrato"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return drop_cont;
    }

    public Contrato buscarContrato(String buscartxt) {
        int r = 0;
        Contrato cont = new Contrato();
        String sql = "SELECT cont.cliente_contrato, cont.fiscal_contrato, cont.codigo_contrato, cont.validez_contrato, cont.fecha_inicio_contrato, cont.fecha_final_contrato,\n"
                + "cont.tipo_contrato, cont.estado_contrato, cont.nombre_usuario_modificacion, cont.fecha_modificacion, clie.descripcion_cliente\n"
                + "FROM contrato cont\n"
                + "INNER JOIN cliente clie\n"
                + "ON cont.id_cliente = clie.id_cliente\n"
                + "WHERE cont.cliente_contrato =''";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, buscartxt);

            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;
                cont.setCliente_contrato(rs.getString("cliente_contrato"));
                cont.setFiscal_contrato(rs.getString("fiscal_contrato"));
                cont.setCodigo_contrato(rs.getString("codigo_contrato"));
                cont.setValidez_contrato(rs.getString("validez_contrato"));
                cont.setFecha_inicio(rs.getString("fecha_inicio_contrato"));
                cont.setFecha_fin(rs.getString("fecha_final_contrato"));
                cont.setTipo_contrato(rs.getString("tipo_contrato"));
                cont.setEstado_contrato(rs.getString("estado_contrato"));
                cont.setNom_usu(rs.getString("nombre_usuario_modificacion"));
                cont.setDescripcion_cliente(rs.getString("descripcion_cliente"));

            }

        } catch (Exception e) {
            System.out.println("hola");

        } finally {
            return cont;
        }
    }

}
