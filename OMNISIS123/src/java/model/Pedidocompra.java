/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 *
 * @author Alvaro
 */
public class Pedidocompra implements ValidarPedidoCompra {

    int id_pedido, id_pedido_detalle, id_servicio, id_usuario, id_producto, cantidad_pedido_detalle;
    String numero_pedido, fecha_pedido, estado_pedido, descripcion_detalle;

    public Pedidocompra() {
    }

    public Pedidocompra(int id_pedido, int id_pedido_detalle, int id_servicio, int id_usuario, int id_producto, int cantidad_pedido_detalle, String numero_pedido, String fecha_pedido, String estado_pedido, String descripcion_detalle) {
        this.id_pedido = id_pedido;
        this.id_pedido_detalle = id_pedido_detalle;
        this.id_servicio = id_servicio;
        this.id_usuario = id_usuario;
        this.id_producto = id_producto;
        this.cantidad_pedido_detalle = cantidad_pedido_detalle;
        this.numero_pedido = numero_pedido;
        this.fecha_pedido = fecha_pedido;
        this.estado_pedido = estado_pedido;
        this.descripcion_detalle = descripcion_detalle;
    }

    public int getId_pedido() {
        return id_pedido;
    }

    public void setId_pedido(int id_pedido) {
        this.id_pedido = id_pedido;
    }

    public int getId_pedido_detalle() {
        return id_pedido_detalle;
    }

    public void setId_pedido_detalle(int id_pedido_detalle) {
        this.id_pedido_detalle = id_pedido_detalle;
    }

    public int getId_servicio() {
        return id_servicio;
    }

    public void setId_servicio(int id_servicio) {
        this.id_servicio = id_servicio;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public String getNumero_pedido() {
        return numero_pedido;
    }

    public void setNumero_pedido(String numero_pedido) {
        this.numero_pedido = numero_pedido;
    }

    public String getFecha_pedido() {
        return fecha_pedido;
    }

    public void setFecha_pedido(String fecha_pedido) {
        this.fecha_pedido = fecha_pedido;
    }

    public String getEstado_pedido() {
        return estado_pedido;
    }

    public void setEstado_pedido(String estado_pedido) {
        this.estado_pedido = estado_pedido;
    }

    public String getDescripcion_detalle() {
        return descripcion_detalle;
    }

    public void setDescripcion_detalle(String descripcion_detalle) {
        this.descripcion_detalle = descripcion_detalle;
    }

    public int getCantidad_pedido_detalle() {
        return cantidad_pedido_detalle;
    }

    public void setCantidad_pedido_detalle(int cantidad_pedido_detalle) {
        this.cantidad_pedido_detalle = cantidad_pedido_detalle;
    }

    Connection con;
    Conexion cn = new Conexion();
    PreparedStatement ps;
    PreparedStatement ps2;
    PreparedStatement ps3;
    PreparedStatement ps4;
    ResultSet rs;
    ResultSet rs2;
    private String sSQL = "";
    private String sSQL2 = "";

    public int registrarPedidoCompra1(Pedidocompra pc) {
        int r = 0;
        int q1 = 0;

        try {

            con = cn.getConnection();
            //INSERTA EL REGISTRO A LA CABECERA
            String sql = "INSERT INTO public.pedido(\n"
                    + "fecha_pedido_producto,nro_pedido,estado_pedido,id_usuario,id_servicio_tecnico)\n"
                    + "VALUES (?, ?, ?, ?, ?);";
            ps = con.prepareStatement(sql);

            ps.setString(1, pc.getFecha_pedido());
            ps.setString(2, pc.getNumero_pedido());
            ps.setString(3, pc.getEstado_pedido());
            ps.setInt(4, pc.getId_usuario());
            ps.setInt(5, pc.getId_servicio());

            ps.executeUpdate();
            //COMENZARA A INGRESAR EL DETALLE
            //AQUI COMIENZA EL WHILE
            String sql1 = "INSERT INTO public.pedido_detalle(descripcion_pedido_parte_detalle,cantidad_pedido_producto_detalle,"
                    + "id_producto,id_pedido)\n"
                    + "VALUES (?, ?, ?, (select MAX(id_pedido) from pedido))";
            ps2 = con.prepareStatement(sql1);
            ps2.setString(1, pc.getDescripcion_detalle());
            ps2.setInt(2, pc.getCantidad_pedido_detalle());
            ps2.setInt(3, pc.getId_producto());
//CUANDO TENGA UN ARRAY DEBE INGRESAR EL PS2 EN UN WHILE
            ps2.executeUpdate();
//AQUI TERMINA EL WHILE
            return 1;
//
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return 0;
        }

    }

    public int registrar(Pedidocompra pc) {

        try {
            con = cn.getConnection();

            String sql1 = "INSERT INTO public.pedido_detalle(descripcion_pedido_parte_detalle,cantidad_pedido_producto_detalle,"
                    + "id_producto,id_pedido)\n"
                    + "VALUES (?, ?, ?, (select MAX(id_pedido) from pedido))";
            ps2 = con.prepareStatement(sql1);
            ps2.setString(1, pc.getDescripcion_detalle());
            ps2.setInt(2, pc.getCantidad_pedido_detalle());
            ps2.setInt(3, pc.getId_producto());
            ps2.executeUpdate();
            return 1;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return 0;
        }
    }

    public HashMap seleccionarPedidocompra() {
        HashMap<String, String> dropped_ped = new HashMap();
        try {
            ConectaBD conn = new ConectaBD();
            String sql = "SELECT ped.id_pedido,  pedet.descripcion_pedido_parte_detalle as id_ped\n"
                    + "	FROM public.pedido ped\n"
                    + "	INNER JOIN pedido_detalle pedet\n"
                    + "	ON pedet.id_pedido = ped.id_pedido\n"
                    + "	Where estado_pedido = 'activo';";
            conn.abrir();
            rs = conn.con.createStatement().executeQuery(sql);
            while (rs.next()) {
                dropped_ped.put(rs.getString("id_pedido"), rs.getString("id_ped"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return dropped_ped;
    }

    public Pedidocompra buscarPedido(String buscartxt) {
        int r = 0;
        Pedidocompra ped = new Pedidocompra();
        String sql = "SELECT ped.fecha_pedido_producto, ped.nro_pedido, ped.estado_pedido,\n"
                + "usu.id_usuario, ser.id_servicio_tecnico, pdet.descripcion_pedido_parte_detalle\n"
                + " pdet.cantidad_pedido_parte_detalle,pdet.id_producto\n"
                + "FROM  pedido ped\n"
                + "INNER JOIN usuario usu on \n"
                + " On"
                + "INNER JOIN pedido_detalle pdet\n"
                + "ON pais.id_pais = prov.id_proveedor\n"
                + "WHERE prov.nombre_proveedor like ?";
//                      FROM servicio_tecnico ser\n"
//                + "    INNER JOIN tipo_servicio_tecnico ts on ser.id_tipo_servicio_tecnico = ts.id_tipo_servicio_tecnico\n"
//                + "    INNER JOIN usuario usu  on ser.id_usuario = usu.id_usuario\n"
//                + "    INNER JOIN contrato cont on ser.id_contrato = cont.id_contrato\n"
//                + "	INNER JOIN cliente clie on cont.id_cliente = clie.id_cliente\n"
//                + "    INNER JOIN impuesto imp on ser.id_impuesto = imp.id_impuesto\n"
//                + "    WHERE ser.numero_servicio_tecnico = ?";
//        "fecha_pedido_producto,nro_pedido,estado_pedido,id_usuario,id_servicio_tecnico)\n"
//                + "VALUES (?, ?, ?, ?, ?)";
//        String sql1 = "INSERT INTO public.pedido_detalle(\n"
//                + "descripcion_pedido_parte_detalle,cantidad_pedido_parte_detalle,id_producto,id_pedido)\n"
//                + "VALUES (?, ?, ?, ?)";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, buscartxt);

            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;
                ped.setFecha_pedido(rs.getString("nombre_proveedor"));
                ped.setNumero_pedido(rs.getString("procedencia_proveedor"));
                ped.setEstado_pedido(rs.getString("ruc_proveedor"));
                ped.setId_usuario(rs.getInt("telefono_proveedor"));
                ped.setId_servicio(rs.getInt("direccion_proveedor"));
                ped.setDescripcion_detalle(rs.getString("descripcion_pais"));
                ped.setCantidad_pedido_detalle(rs.getInt("descripcion_pais"));
                ped.setId_producto(rs.getInt("descripcion_pais"));
            }

        } catch (Exception e) {
            System.out.println("hola");

        } finally {
            return ped;
        }

    }

}
