/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Francisca Gómez
 */
public class Fp {

    int id, id_usu, id_ser;
    String fecha, nro, estado;
    private Connection conn;
    private Statement stm;
    private ResultSet rs;
    private Pedido prestamHallado1;

    public ArrayList<Pedido> leerPedidpDet(int codigo) {
        ArrayList<Pedido> prestamos = new ArrayList<Pedido>();
        try {
            conn = ConectaBD.abrir();
            stm = conn.createStatement();
            //prestamResSet3 = stm.executeQuery ("SELECT nombre FROM public.usuario where id_usuario = '1'");

            rs = stm.executeQuery("SELECT id_pedido, fecha_pedido_producto, nro_pedido, estado_pedido, id_usuario, id_servicio_tecnico\n"
                    + "	FROM public.pedido\n"
                    + "	WHERE id_pedido = " + codigo + "");
            if (!rs.next()) {
                System.out.println("No se encontro el registro");
                ConectaBD.cerrar();
                return prestamos;

            } else {
                do {
                    id = rs.getInt("id_pedido");
                    fecha = rs.getString("fecha_pedido_producto");
                    nro = rs.getString("nro_pedido");
                    estado = rs.getString("estado_pedido");
                    id_usu = rs.getInt("id_usuario");
                    id_ser = rs.getInt("id_servicio_tecnico");
                    prestamHallado1 = new Pedido(codigo, id, id_usu, id_ser, fecha, nro, estado);
                    prestamos.add(prestamHallado1);
                } while (rs.next());
                ConectaBD.cerrar();
                return prestamos;
            }

        } catch (Exception e) {
            System.out.println("Error en la base de datos");
            e.printStackTrace();
            return null;
        }
    }
}
