/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 *
 * @author Francisca Gómez
 */
public class Perfil implements ValidarPerfil {

    //CONSTRUCTORES - SET - GET
    int id_perfil;
    String descrip_perfil;

    public Perfil(int id_perfil, String descrip_perfil) {
        this.id_perfil = id_perfil;
        this.descrip_perfil = descrip_perfil;
    }

    public Perfil() {
    }

    public int getId_perfil() {
        return id_perfil;
    }

    public void setId_perfil(int id_perfil) {
        this.id_perfil = id_perfil;
    }

    public String getDescrip_perfil() {
        return descrip_perfil;
    }

    public void setDescrip_perfil(String descrip_perfil) {
        this.descrip_perfil = descrip_perfil;
    }

    //FUNCIONES REGISTRAR - BUSCAR - MODIFICAR - CAMBIAR ESTADO
    Connection con;
    Conexion cn = new Conexion();
    PreparedStatement ps;
    ResultSet rs;

    @Override
    public int registrarPerfil(Perfil per) {
        int r = 0;
        String sql = "INSERT INTO public.perfil(\n"
                + "descripcion_perfil)\n"
                + "VALUES (?);";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, per.getDescrip_perfil());
            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;
                per.setDescrip_perfil(rs.getString("descripcion_perfil"));
            }
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }

        } catch (Exception e) {
            return 0;
        }
    }

    public HashMap seleccionarPerfil() {
        HashMap<String, String> drop_per = new HashMap();
        try {
            ConectaBD conn = new ConectaBD();
            String sql = "SELECT id_perfil as id_per,descripcion_perfil\n"
                    + "FROM public.perfil;";
            conn.abrir();
            rs = conn.con.createStatement().executeQuery(sql);
            while (rs.next()) {
                drop_per.put(rs.getString("id_per"), rs.getString("descripcion_perfil"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return drop_per;
    }

    public Perfil buscarPerfil(String buscartxt) {
        int r = 0;
        Perfil per = new Perfil();
        String sql = "SELECT descripcion_perfil\n"
                + "FROM perfil\n"
                + "WHERE descripcion_perfil = ?;";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, buscartxt);

            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;
                per.setDescrip_perfil(rs.getString("descripcion_perfil"));
            }

        } catch (Exception e) {
            System.out.println("hola");

        } finally {
            return per;
        }
    }

    public int modificarPerfil(Perfil per) {
        int r = 0;
        String sql = "UPDATE public.perfil\n"
                + "SET descripcion_perfil=?\n"
                + "WHERE id_perfil=?;";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, per.getDescrip_perfil());
            ps.setInt(2, per.getId_perfil());
            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;
                per.setDescrip_perfil(rs.getString("descripcion_perfil"));
            }
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            return 0;
        }
    }
}
