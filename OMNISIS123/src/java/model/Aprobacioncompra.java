/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 *
 * @author Alvaro
 */
public class Aprobacioncompra implements ValidarAprobacionCompra {

    int id_aprobacion, id_usuario, id_pedido;
    String descripcion_aprobacion_compra, nro_aprobacion, estado_aprobacion, nom_usu, fecha_mod, nro_pedido;

    public Aprobacioncompra() {
    }

    public Aprobacioncompra(int id_aprobacion, int id_usuario, int id_pedido, String descripcion_aprobacion_compra, String nro_aprobacion, String estado_aprobacion, String nom_usu, String fecha_mod, String nro_pedido) {
        this.id_aprobacion = id_aprobacion;
        this.id_usuario = id_usuario;
        this.id_pedido = id_pedido;
        this.descripcion_aprobacion_compra = descripcion_aprobacion_compra;
        this.nro_aprobacion = nro_aprobacion;
        this.estado_aprobacion = estado_aprobacion;
        this.nom_usu = nom_usu;
        this.fecha_mod = fecha_mod;
        this.nro_pedido = nro_pedido;
    }

    public int getId_aprobacion() {
        return id_aprobacion;
    }

    public void setId_aprobacion(int id_aprobacion) {
        this.id_aprobacion = id_aprobacion;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public int getId_pedido() {
        return id_pedido;
    }

    public void setId_pedido(int id_pedido) {
        this.id_pedido = id_pedido;
    }

    public String getDescripcion_aprobacion_compra() {
        return descripcion_aprobacion_compra;
    }

    public void setDescripcion_aprobacion_compra(String descripcion_aprobacion_compra) {
        this.descripcion_aprobacion_compra = descripcion_aprobacion_compra;
    }

    public String getNro_aprobacion() {
        return nro_aprobacion;
    }

    public void setNro_aprobacion(String nro_aprobacion) {
        this.nro_aprobacion = nro_aprobacion;
    }

    public String getEstado_aprobacion() {
        return estado_aprobacion;
    }

    public void setEstado_aprobacion(String estado_aprobacion) {
        this.estado_aprobacion = estado_aprobacion;
    }

    public String getNom_usu() {
        return nom_usu;
    }

    public void setNom_usu(String nom_usu) {
        this.nom_usu = nom_usu;
    }

    public String getFecha_mod() {
        return fecha_mod;
    }

    public void setFecha_mod(String fecha_mod) {
        this.fecha_mod = fecha_mod;
    }

    public String getNro_pedido() {
        return nro_pedido;
    }

    public void setNro_pedido(String nro_pedido) {
        this.nro_pedido = nro_pedido;
    }

    Connection con;
    Conexion cn = new Conexion();
    PreparedStatement ps;
    ResultSet rs;
    private String sSQL = "";

    @Override
    public int registrarAprobacioncompra(Aprobacioncompra apr) {
        int r = 0;

        try {
            con = cn.getConnection();
            String sql = "INSERT INTO public.aprobacion_compra(\n"
                    + "descripcion_aprobacion_compra, nro_aprobacion,\n"
                    + "estado_aprobacion_compra, nombre_usuario_modificacion, fecha_modificacion,\n"
                    + "id_usuario, id_pedido)\n"
                    + "VALUES (?, ?, ?, ?, (SELECT current_date), ?, ?);";
            ps = con.prepareStatement(sql);

            ps.setString(1, apr.getDescripcion_aprobacion_compra());
            ps.setString(2, apr.getNro_aprobacion());
            ps.setString(3, apr.getEstado_aprobacion());
            ps.setString(4, apr.getNom_usu());
            ps.setInt(5, apr.getId_usuario());
            ps.setInt(6, apr.getId_pedido());

            ps.executeUpdate();

            return 1;

        } catch (Exception e) {
            return 0;
        }
    }

    public HashMap seleccionarAprobacion() {
        HashMap<String, String> dropapro_apro = new HashMap();
        try {
            ConectaBD conn = new ConectaBD();
            String sql = "SELECT id_aprobacion_compra as id_apr,descripcion_aprobacion_compra\n"
                    + "	FROM public.aprobacion_compra;";
            conn.abrir();
            rs = conn.con.createStatement().executeQuery(sql);
            while (rs.next()) {
                dropapro_apro.put(rs.getString("id_apr"), rs.getString("descripcion_aprobacion_compra"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return dropapro_apro;
    }

    public Aprobacioncompra buscarAprobacioncompra(String buscartxt) {
        int r = 0;
        Aprobacioncompra apr = new Aprobacioncompra();
        String sql = "SELECT apr.id_aprobacion_compra, apr.descripcion_aprobacion_compra,\n"
                + "apr.nro_aprobacion, apr.estado_aprobacion_compra,\n"
                + "usu.id_usuario, ped.id_pedido\n"
                + "FROM public.aprobacion_compra apr\n"
                + "INNER JOIN usuario usu\n"
                + "ON usu.id_usuario = apr.id_usuario\n"
                + "INNER JOIN pedido ped\n"
                + "ON ped.id_pedido = apr.id_pedido\n"
                + "WHERE apr.nro_aprobacion = ?";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, buscartxt);
            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;
                apr.setDescripcion_aprobacion_compra(rs.getString("descripcion_aprobacion_compra"));
                apr.setNro_aprobacion(rs.getString("nro_aprobacion"));
                apr.setEstado_aprobacion(rs.getString("estado_aprobacion_compra"));
                apr.setId_usuario(rs.getInt("id_usuario"));
                apr.setId_pedido(rs.getInt("id_pedido"));
                apr.setId_aprobacion(rs.getInt("id_aprobacion_compra"));
            }
        } catch (Exception e) {
            System.out.println("hola");
        } finally {
            return apr;
        }
    }

    public int modificarAprobacion(Aprobacioncompra apr) {
        int r = 0;
        String sql = "UPDATE public.aprobacion_compra\n"
                + "SET descripcion_aprobacion_compra=?, nro_aprobacion=?, estado_aprobacion_compra=?, \n"
                + "nombre_usuario_modificacion=?, fecha_modificacion=(SELECT current_date), id_usuario=?, id_pedido=?\n"
                + "WHERE id_aprobacion_compra=?;";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, apr.getDescripcion_aprobacion_compra());
            ps.setString(2, apr.getNro_aprobacion());
            ps.setString(3, apr.getEstado_aprobacion());
            ps.setString(4, apr.getNom_usu());
            ps.setInt(5, apr.getId_usuario());
            ps.setInt(6, apr.getId_pedido());
            ps.setInt(7, apr.getId_aprobacion());
            ps.executeUpdate();
            
            return 1;
            
        } catch (Exception e) {
            return 0;
        }
    }
}
