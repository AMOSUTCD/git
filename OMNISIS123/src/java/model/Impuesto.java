/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 *
 * @author Francisca Gómez
 */
public class Impuesto implements ValidarImpuesto {

    //CONSTRUCTORES - SET - GET
    int id_impuesto;
    String descripcion_impuesto;
    double cifra;
    String nombre_usuario_modificacion;

    public Impuesto() {
    }

    public int getId_impuesto() {
        return id_impuesto;
    }

    public void setId_impuesto(int id_impuesto) {
        this.id_impuesto = id_impuesto;
    }

    public String getDescripcion_impuesto() {
        return descripcion_impuesto;
    }

    public void setDescripcion_impuesto(String descripcion_impuesto) {
        this.descripcion_impuesto = descripcion_impuesto;
    }

    public double getCifra() {
        return cifra;
    }

    public void setCifra(double cifra) {
        this.cifra = cifra;
    }

    public String getNombre_usuario_modificacion() {
        return nombre_usuario_modificacion;
    }

    public void setNombre_usuario_modificacion(String nombre_usuario_modificacion) {
        this.nombre_usuario_modificacion = nombre_usuario_modificacion;
    }

    //FUNCIONES REGISTRAR - BUSCAR - MODIFICAR - CAMBIAR ESTADO
    Connection con;
    Conexion cn = new Conexion();
    PreparedStatement ps;
    ResultSet rs;

    @Override
    public int registrarImpuesto(Impuesto im) {
        int r = 0;
        String sql = "INSERT INTO public.impuesto(descripcion_impuesto, cifra, nombre_usuario_modificacion, fecha_modificacion)\n"
                + "	VALUES (?,?,?,(SELECT current_date))";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, im.getDescripcion_impuesto());
            ps.setDouble(2, im.getCifra());
            ps.setString(3, im.getNombre_usuario_modificacion());
            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;
                im.setDescripcion_impuesto(rs.getString("descripcion_impuesto"));
                im.setCifra(rs.getDouble("cifra"));
                im.setNombre_usuario_modificacion(rs.getString("nombre_usuario_modificacion"));
            }
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            return 0;
        }
    }

    public HashMap seleccionarImpuesto() {
        HashMap<String, String> drop_imp = new HashMap();
        try {
            ConectaBD conn = new ConectaBD();
            String sql = "SELECT id_impuesto as id_i,descripcion_impuesto\n"
                    + "	FROM public.impuesto;";
            conn.abrir();
            rs = conn.con.createStatement().executeQuery(sql);
            while (rs.next()) {
                drop_imp.put(rs.getString("id_i"), rs.getString("descripcion_impuesto"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return drop_imp;
    }

    public Impuesto buscarImpuesto(String buscartxt) {
        int r = 0;
        Impuesto im = new Impuesto();
        String sql = "SELECT id_impuesto, descripcion_impuesto,cifra\n"
                + "FROM impuesto\n"
                + "WHERE descripcion_impuesto = ?;";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, buscartxt);
            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;
                im.setId_impuesto(rs.getInt("id_impuesto"));
                im.setDescripcion_impuesto(rs.getString("descripcion_impuesto"));
                im.setCifra(rs.getDouble("cifra"));
            }
        } catch (Exception e) {
            System.out.println("hola");

        } finally {
            return im;
        }
    }

    public int modificarImpuesto(Impuesto im) {
        int r = 0;
        String sql = "UPDATE public.impuesto\n"
                + "SET descripcion_impuesto=?, cifra=?, nombre_usuario_modificacion=?, fecha_modificacion=(SELECT current_date)\n"
                + "WHERE id_impuesto=?;";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, im.getDescripcion_impuesto());
            ps.setDouble(2, im.getCifra());
            ps.setString(3, im.getNombre_usuario_modificacion());
            ps.setInt(4, im.getId_impuesto());
            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;
                im.setDescripcion_impuesto(rs.getString("descripcion_impuesto"));
                im.setCifra(rs.getDouble("cifra"));
            }
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            return 0;
        }
    }
}
