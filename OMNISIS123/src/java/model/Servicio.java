/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 *
 * @author Alvaro
 */
public class Servicio implements ValidarServicio {

    int id_servicio_tecnico, id_tipo_servicio_tecnico, id_usuario, id_contrato, id_impuesto;
    String numero_servicio_tecnico, descripcion_servicio_tecnico, iva_servicio_tecnico, fecha_servicio_tecnnico, estado_servicio_tecnico, 
            nombre_usuario_modificacion, fecha_modificacion, usuario_carga, notas, cliente_contrato, descr_impuesto, cod_contrado, descr_tipo_ser;
    double precio_servicio_tecnico;

    public Servicio() {
    }

    public Servicio(int id_servicio_tecnico, int id_tipo_servicio_tecnico, int id_usuario, int id_contrato, int id_impuesto, String numero_servicio_tecnico, String descripcion_servicio_tecnico, String iva_servicio_tecnico, String fecha_servicio_tecnnico, String estado_servicio_tecnico, String nombre_usuario_modificacion, String fecha_modificacion, String usuario_carga, String notas, String cliente_contrato, String descr_impuesto, String cod_contrado, String descr_tipo_ser, double precio_servicio_tecnico) {
        this.id_servicio_tecnico = id_servicio_tecnico;
        this.id_tipo_servicio_tecnico = id_tipo_servicio_tecnico;
        this.id_usuario = id_usuario;
        this.id_contrato = id_contrato;
        this.id_impuesto = id_impuesto;
        this.numero_servicio_tecnico = numero_servicio_tecnico;
        this.descripcion_servicio_tecnico = descripcion_servicio_tecnico;
        this.iva_servicio_tecnico = iva_servicio_tecnico;
        this.fecha_servicio_tecnnico = fecha_servicio_tecnnico;
        this.estado_servicio_tecnico = estado_servicio_tecnico;
        this.nombre_usuario_modificacion = nombre_usuario_modificacion;
        this.fecha_modificacion = fecha_modificacion;
        this.usuario_carga = usuario_carga;
        this.notas = notas;
        this.cliente_contrato = cliente_contrato;
        this.descr_impuesto = descr_impuesto;
        this.cod_contrado = cod_contrado;
        this.descr_tipo_ser = descr_tipo_ser;
        this.precio_servicio_tecnico = precio_servicio_tecnico;
    }

    

    public int getId_servicio_tecnico() {
        return id_servicio_tecnico;
    }

    public void setId_servicio_tecnico(int id_servicio_tecnico) {
        this.id_servicio_tecnico = id_servicio_tecnico;
    }

    public int getId_tipo_servicio_tecnico() {
        return id_tipo_servicio_tecnico;
    }

    public void setId_tipo_servicio_tecnico(int id_tipo_servicio_tecnico) {
        this.id_tipo_servicio_tecnico = id_tipo_servicio_tecnico;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public int getId_contrato() {
        return id_contrato;
    }

    public void setId_contrato(int id_contrato) {
        this.id_contrato = id_contrato;
    }

    public int getId_impuesto() {
        return id_impuesto;
    }

    public void setId_impuesto(int id_impuesto) {
        this.id_impuesto = id_impuesto;
    }

    public String getNumero_servicio_tecnico() {
        return numero_servicio_tecnico;
    }

    public void setNumero_servicio_tecnico(String numero_servicio_tecnico) {
        this.numero_servicio_tecnico = numero_servicio_tecnico;
    }

    public String getDescripcion_servicio_tecnico() {
        return descripcion_servicio_tecnico;
    }

    public void setDescripcion_servicio_tecnico(String descripcion_servicio_tecnico) {
        this.descripcion_servicio_tecnico = descripcion_servicio_tecnico;
    }

    public String getIva_servicio_tecnico() {
        return iva_servicio_tecnico;
    }

    public void setIva_servicio_tecnico(String iva_servicio_tecnico) {
        this.iva_servicio_tecnico = iva_servicio_tecnico;
    }

    public String getFecha_servicio_tecnnico() {
        return fecha_servicio_tecnnico;
    }

    public void setFecha_servicio_tecnnico(String fecha_servicio_tecnnico) {
        this.fecha_servicio_tecnnico = fecha_servicio_tecnnico;
    }

    public String getEstado_servicio_tecnico() {
        return estado_servicio_tecnico;
    }

    public void setEstado_servicio_tecnico(String estado_servicio_tecnico) {
        this.estado_servicio_tecnico = estado_servicio_tecnico;
    }

    public String getNombre_usuario_modificacion() {
        return nombre_usuario_modificacion;
    }

    public void setNombre_usuario_modificacion(String nombre_usuario_modificacion) {
        this.nombre_usuario_modificacion = nombre_usuario_modificacion;
    }

    public String getFecha_modificacion() {
        return fecha_modificacion;
    }

    public void setFecha_modificacion(String fecha_modificacion) {
        this.fecha_modificacion = fecha_modificacion;
    }

    public String getUsuario_carga() {
        return usuario_carga;
    }

    public void setUsuario_carga(String usuario_carga) {
        this.usuario_carga = usuario_carga;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public String getCliente_contrato() {
        return cliente_contrato;
    }

    public void setCliente_contrato(String cliente_contrato) {
        this.cliente_contrato = cliente_contrato;
    }

    public double getPrecio_servicio_tecnico() {
        return precio_servicio_tecnico;
    }

    public void setPrecio_servicio_tecnico(double precio_servicio_tecnico) {
        this.precio_servicio_tecnico = precio_servicio_tecnico;
    }

    public String getDescr_impuesto() {
        return descr_impuesto;
    }

    public void setDescr_impuesto(String descr_impuesto) {
        this.descr_impuesto = descr_impuesto;
    }

    public String getCod_contrado() {
        return cod_contrado;
    }

    public void setCod_contrado(String cod_contrado) {
        this.cod_contrado = cod_contrado;
    }

    public String getDescr_tipo_ser() {
        return descr_tipo_ser;
    }

    public void setDescr_tipo_ser(String descr_tipo_ser) {
        this.descr_tipo_ser = descr_tipo_ser;
    }

    Connection con;
    Conexion cn = new Conexion();
    PreparedStatement ps;
    ResultSet rs;

    @Override
    public int registrarServicio(Servicio ser) {
        int r = 0;
        String sql = "INSERT INTO public.servicio_tecnico(\n"
                + "numero_servicio_tecnico, descripcion_servicio_tecnico, precio_servicio_tecnico, iva_servicio_tecnico,\n"
                + "fecha_servicio_tecnico, estado_servicio_tecnico, nombre_usuario_modificacion, fecha_modificacion, id_tipo_servicio_tecnico, id_usuario, id_contrato, id_impuesto, usuario_carga, notas, cliente_contrato)\n"
                + "VALUES (?, ?, ?, ?, ?, ?, ?, (SELECT current_date), ?, ?, ?, ?, ?, ?, ?)";

//                "INSERT INTO public.servicio_tecnico(\n"
//                + "numero_servicio_tecnico, descripcion_servicio_tecnico, precio_servicio_tecnico, iva_servicio_tecnico, fecha_servicio_tecnico, estado_servicio_tecnico, \n"
//                + "nombre_usuario_modificacion, fecha_modificacion, id_tipo_servicio_tecnico, id_usuario, id_contrato,id_impuesto, usuario_carga, notas, cliente_contrato)\n"
//                + "VALUES (?, ?, ?, ?, ?, ?, ?, (SELECT current_date), ?, ?, ?, ?, ?, ?, ?);";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, ser.getNumero_servicio_tecnico());
            ps.setString(2, ser.getDescripcion_servicio_tecnico());
            ps.setDouble(3, ser.getPrecio_servicio_tecnico());
            ps.setString(4, ser.getIva_servicio_tecnico());
            ps.setString(5, ser.getFecha_servicio_tecnnico());
            ps.setString(6, ser.getEstado_servicio_tecnico());
            ps.setString(7, ser.getNombre_usuario_modificacion());
            ps.setInt(8, ser.getId_tipo_servicio_tecnico());
            ps.setInt(9, ser.getId_usuario());
            ps.setInt(10, ser.getId_contrato());
            ps.setInt(11, ser.getId_impuesto());
            ps.setString(12, ser.getUsuario_carga());
            ps.setString(13, ser.getNotas());
            ps.setString(14, ser.getCliente_contrato());

            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;
                //Variables.id = Integer.parseInt(rs.getString("id_usuario"));
                ser.setNumero_servicio_tecnico(rs.getString("numero_servicio_tecnico"));
                ser.setDescripcion_servicio_tecnico(rs.getString("descripcion_servicio_tecnico"));
                ser.setPrecio_servicio_tecnico(rs.getDouble("precio_servicio_tecnico"));
                ser.setIva_servicio_tecnico(rs.getString("iva_servicio_tecnico"));
                ser.setFecha_servicio_tecnnico(rs.getString("fecha_servicio_tecnico"));
                ser.setEstado_servicio_tecnico(rs.getString("estado_servicio_tecnico"));
                ser.setNombre_usuario_modificacion(rs.getString("nombre_usuario_modificacion"));
                ser.setId_tipo_servicio_tecnico(rs.getInt("id_tipo_servicio_tecnico"));
                ser.setId_usuario(rs.getInt("id_usuario"));
                ser.setId_contrato(rs.getInt("id_contrato"));
                ser.setId_impuesto(rs.getInt("id_impuesto"));
                ser.setUsuario_carga(rs.getString("usuario_carga"));
                ser.setNotas(rs.getString("notas"));
                ser.setCliente_contrato(rs.getString("cliente_contrato"));

            }
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return 0;

        }

    }

    public HashMap seleccionarServicio() {
        HashMap<String, String> drop_ser = new HashMap();
        try {
            ConectaBD conn = new ConectaBD();
            String sql = "SELECT id_servicio_tecnico as id_ser,descripcion_servicio_tecnico\n"
                    + "                    FROM public.servicio_tecnico";
            //   + "                    WHERE cliente_contrato = ?;";
            conn.abrir();
            rs = conn.con.createStatement().executeQuery(sql);
            while (rs.next()) {
                drop_ser.put(rs.getString("id_ser"), rs.getString("descripcion_servicio_tecnico"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return drop_ser;
    }

    public Servicio buscarServicio(String buscartxt) {
        int r = 0;
        Servicio ser = new Servicio();
        String sql = "SELECT ser.numero_servicio_tecnico, ser.descripcion_servicio_tecnico, ser.precio_servicio_tecnico, ser.iva_servicio_tecnico, ser.fecha_servicio_tecnico, ser.estado_servicio_tecnico,\n"
                + "ser.usuario_carga, ts.descripcion_tipo_servicio_tecnico, cont.codigo_contrato,imp.descripcion_impuesto, ser.notas, clie.descripcion_cliente\n"
                + "FROM servicio_tecnico ser\n"
                + "INNER JOIN tipo_servicio_tecnico ts on ser.id_tipo_servicio_tecnico = ts.id_tipo_servicio_tecnico\n"
                + "INNER JOIN usuario usu  on ser.id_usuario = usu.id_usuario\n"
                + "INNER JOIN contrato cont on ser.id_contrato = cont.id_contrato\n"
                + "INNER JOIN cliente clie on cont.id_cliente = clie.id_cliente\n"
                + "INNER JOIN impuesto imp on ser.id_impuesto = imp.id_impuesto\n"
                + "WHERE ser.numero_servicio_tecnico = ?";

        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, buscartxt);

            rs = ps.executeQuery();
            while (rs.next()) {
                r = r + 1;

                ser.setNumero_servicio_tecnico(rs.getString("numero_servicio_tecnico"));
                ser.setDescripcion_servicio_tecnico(rs.getString("descripcion_servicio_tecnico"));
                ser.setPrecio_servicio_tecnico(rs.getDouble("precio_servicio_tecnico"));
                ser.setIva_servicio_tecnico(rs.getString("iva_servicio_tecnico"));
                ser.setFecha_servicio_tecnnico(rs.getString("fecha_servicio_tecnico"));
                ser.setEstado_servicio_tecnico(rs.getString("estado_servicio_tecnico"));
                ser.setNombre_usuario_modificacion(rs.getString("usuario_carga"));
                ser.setDescr_tipo_ser(rs.getString("descripcion_tipo_servicio_tecnico"));
                ser.setCod_contrado(rs.getString("codigo_contrato"));
                ser.setDescr_impuesto(rs.getString("descripcion_impuesto"));
                ser.setNotas(rs.getString("notas"));
                ser.setCliente_contrato(rs.getString("descripcion_cliente"));

            }

        } catch (Exception e) {
            System.out.println("hola");

        } finally {
            return ser;
        }
    }

}
