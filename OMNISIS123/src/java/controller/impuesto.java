/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Impuesto;
import model.Variables;

/**
 *
 * @author Francisca Gómez
 */
@WebServlet(name = "impuesto", urlPatterns = {"/impuesto"})
public class impuesto extends HttpServlet {

    Impuesto im = new Impuesto();
    Impuesto imbd = new Impuesto();
    int r;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String accion = request.getParameter("accion");
        if (accion.equals("Guardar")) {
            Double cifra1;
            String descrip = request.getParameter("descripimptxt");
            String cifra = request.getParameter("cifratxt");
            cifra1 = Double.parseDouble(cifra);
            String usumod = Variables.usuario1;
            int length = descrip.length();
            if (length == 0) {
                request.getRequestDispatcher("mensaje.jsp").forward(request, response);
            } else {
                im.setDescripcion_impuesto(descrip);
                im.setCifra(cifra1);
                im.setNombre_usuario_modificacion(usumod);
                r = imbd.registrarImpuesto(im);
                if (r == 0) {
                    request.getRequestDispatcher("registradoReferenciales.jsp").forward(request, response);
                } else {
                    request.getRequestDispatcher("errorGeneral.jsp").forward(request, response);
                }
            }
        }

        if (accion.equals("Buscar")) {
            String buscar = request.getParameter("buscartxt");
            int length = buscar.length();
            im = im.buscarImpuesto(buscar);
            request.setAttribute("Impuesto", im);
            request.getRequestDispatcher("impuesto.jsp").forward(request, response);
        }

        if (accion.equals("Impuesto")) {
            String buscar = request.getParameter("buscartxt");
            int length = buscar.length();
            im = im.buscarImpuesto(buscar);
            request.setAttribute("Impuesto", im);
            request.getRequestDispatcher("impuesto.jsp").forward(request, response);
        }

        if (accion.equals("Modificar")) {
            Double cifra1;
            String descrip = request.getParameter("descripimptxt");
            String cifra = request.getParameter("cifratxt");
            cifra1 = Double.parseDouble(cifra);
            String usumod = Variables.usuario1;
            int id_im = Integer.valueOf(request.getParameter("idtxt"));
            int length = descrip.length();
            if (length == 0) {
                request.getRequestDispatcher("mensaje.jsp").forward(request, response);
            } else {
                im.setDescripcion_impuesto(descrip);
                im.setCifra(cifra1);
                im.setNombre_usuario_modificacion(usumod);
                im.setId_impuesto(id_im);
                r = imbd.modificarImpuesto(im);
                if (r == 0) {
                    request.getRequestDispatcher("registradoReferenciales.jsp").forward(request, response);
                } else {
                    request.getRequestDispatcher("errorGeneral.jsp").forward(request, response);
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
