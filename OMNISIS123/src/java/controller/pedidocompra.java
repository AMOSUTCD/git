/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Pedido;
import model.Pedidocompra;
import model.Variables;

/**
 *
 * @author Alvaro
 */
@WebServlet(name = "pedidocompra", urlPatterns = {"/pedidocompra"})
public class pedidocompra extends HttpServlet {

    Pedidocompra ped = new Pedidocompra();
    Pedidocompra pedbd = new Pedidocompra();
    int r;
    boolean x;
    private Pedido pedidoDetHallado;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        ArrayList<Pedido> pedidoDetalle = new ArrayList();
        Pedido leer = new Pedido();
        //pedidoDetalle = leer.leerPedidoDet();
        pedidoDetalle = leer.leerPedido();

        String accion = request.getParameter("accion");
        if (pedidoDetalle != null) {
            request.setAttribute("PedidoDet", pedidoDetalle);
            if (accion.equals("Ingresar")) {
                String descripcion = request.getParameter("descrtxt");
                int cantidad = Integer.parseInt(request.getParameter("cantxt"));
                int producto = Integer.parseInt(request.getParameter("drop_pro"));
                try {
                    Pedido ped01 = new Pedido();
                    ped01.setDescrip_det(descripcion);
                    ped01.setCant_det(cantidad);
                    ped01.setId_prod(producto);
                    //Pedido agregarDetalle = new Pedido(cantidad, producto, descripcion);
                    //pedidoDetalle.add(agregarDetalle);                   
                    //request.setAttribute("PedidoDet", pedidoDetalle);
                    Variables.addPedidos(ped01);
                    request.setAttribute("PedidoDet", Variables.getPedidos());
                    request.setAttribute("Pedidocompra", ped);
                    request.getRequestDispatcher("pedidocompra.jsp").forward(request, response);                   
                   
                } catch (Exception e) {
                    out.close();
                }
            }

        }
        if (accion.equals("Guardar")) {

            String nro_pedido = request.getParameter("numerotxt");
            String fecha_pedido = request.getParameter("fechapedidotxt");
            String estado_pedido = request.getParameter("customRadio");
            String descripcion = request.getParameter("descrtxt");
            int cantidad = Integer.parseInt(request.getParameter("cantxt"));
            int id_usuario = Integer.valueOf(request.getParameter("drop_usu"));
            int id_servicio = Integer.valueOf(request.getParameter("drop_ser"));
            int producto = Integer.valueOf(request.getParameter("drop_pro"));
            String usu = Variables.usuario1;
            int length = fecha_pedido.length();
            int length2 = nro_pedido.length();
            int length3 = estado_pedido.length();

            if (length == 0 || length2 == 0 || length3 == 0) {
                request.getRequestDispatcher("mensaje.jsp").forward(request, response);
            } else {

                ped.setNumero_pedido(nro_pedido);
                ped.setFecha_pedido(fecha_pedido);
                ped.setEstado_pedido(estado_pedido);
                ped.setDescripcion_detalle(descripcion);
                ped.setCantidad_pedido_detalle(cantidad);
                ped.setId_usuario(id_usuario);
                ped.setId_servicio(id_servicio);
                ped.setId_producto(producto);
                r = pedbd.registrarPedidoCompra1(ped);
                if (r == 1) {  //modificado, porque entraba con 1 al error general jsp
                    request.getRequestDispatcher("pedidocompraguardado.jsp").forward(request, response);

                } else {
                    request.getRequestDispatcher("errorGeneral.jsp").forward(request, response);
                }
            }
        }
        if (accion.equals("Buscar")) {
            String buscar = request.getParameter("buscartxt");
            int length = buscar.length();
            ped = ped.buscarPedido(buscar);
            request.setAttribute("Pedido", ped);
            request.getRequestDispatcher("pedidocompra.jsp").forward(request, response);

        }
        if (accion.equals("Pedido")) {
            String buscar = request.getParameter("buscartxt");
            int length = buscar.length();
            ped = ped.buscarPedido(buscar);
            request.setAttribute("Pedidocompra", ped);
            request.getRequestDispatcher("pedidocompra.jsp").forward(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
