/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Ordencompra;
import model.Variables;

/**
 *
 * @author Alvaro
 */
@WebServlet(name = "ordencompra", urlPatterns = {"/ordencompra"})
public class ordencompra extends HttpServlet {

    Ordencompra ord = new Ordencompra();
    Ordencompra ordbd = new Ordencompra();
    int r;
    boolean x;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String accion = request.getParameter("accion");

        if (accion.equals("Guardar")) {
            
            String nro_orden = request.getParameter("numerotxt");
            String fecha_pedido_orden = request.getParameter("fechapedidotxt");
            String fecha_pago_orden = request.getParameter("fechapagotxt");
            String direccion_orden = request.getParameter("direcciontxt");
            String estado_pedido = request.getParameter("customRadio");
            int id_proveedor = Integer.valueOf(request.getParameter("drop_prov")); 
            int nro_orden_det = Integer.valueOf(request.getParameter("numerotxt"));
            String descripcion_det = request.getParameter("descriptxt");
            int cantidad_det = Integer.valueOf(request.getParameter("cantidadtxt"));
            double precio_unit = Double.valueOf(request.getParameter("preciounittxt"));
            double precio_total = Double.valueOf(request.getParameter("preciototaltxt"));
            double costo = Double.valueOf(request.getParameter("costotxt"));            
            int producto = Integer.valueOf(request.getParameter("droppro_pro"));
            int id_aprobacion = Integer.valueOf(request.getParameter("dropapro_apro"));
            String usu = Variables.usuario1;
            
            int length = fecha_pedido_orden.length();
            int length2 = nro_orden.length();
            int length3 = estado_pedido.length();

            if (length == 0 || length2 == 0 || length3 == 0) {
                request.getRequestDispatcher("mensaje.jsp").forward(request, response);
            } else {
                
                ord.setNro_orden(nro_orden);
                ord.setFecha_pedido_orden(fecha_pedido_orden);
                ord.setFecha_pago_orden(fecha_pago_orden);
                ord.setDireccion_entrega_orden(direccion_orden);
                ord.setEstado_orden(estado_pedido);
                ord.setId_proveedor(id_proveedor);
                ord.setNom_usu(usu);
                ord.setNro_orden_detalle(nro_orden_det);
                ord.setDescrip_orden_det(descripcion_det);
                ord.setCantidad_orden_det(cantidad_det);
                ord.setPrecio_unit_orden_det(precio_unit);
                ord.setPrecio_total_orden_det(precio_total);
                ord.setCosto_total_orden_det(costo);
                ord.setId_producto(producto);
                ord.setId_aprobacion(id_aprobacion);
                
                r = ordbd.registrarOrdencompra(ord);
                if (r == 1) {  //modificado, porque entraba con 1 al error general jsp
                    request.getRequestDispatcher("registradoCompras.jsp").forward(request, response);
                    
                } else {
                    request.getRequestDispatcher("errorGeneral.jsp").forward(request, response);
                }
            }
        }
        if (accion.equals("Buscar")) {
            String buscar = request.getParameter("buscartxt");
            int length = buscar.length();
            ord = ord.buscarOrdencompra(buscar);
            request.setAttribute("Ordencompra", ord);
            request.getRequestDispatcher("ordencompra.jsp").forward(request, response);

        }
        if (accion.equals("Orden")) {
            String buscar = request.getParameter("buscartxt");
            int length = buscar.length();
            ord = ord.buscarOrdencompra(buscar);
            request.setAttribute("Ordencompra", ord);
            request.getRequestDispatcher("ordencompra.jsp").forward(request, response);

        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
