/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Moneda;
import model.Variables;

/**
 *
 * @author Francisca Gómez
 */
@WebServlet(name = "moneda", urlPatterns = {"/moneda"})
public class moneda extends HttpServlet {

    Moneda mon = new Moneda();
    Moneda monbd = new Moneda();
    int r;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String accion = request.getParameter("accion");
        if (accion.equals("Guardar")) {
            String descrip = request.getParameter("descriptxt");
            int cotiCompra = Integer.valueOf(request.getParameter("cotiCompratxt"));
            int cotiVenta = Integer.valueOf(request.getParameter("cotiVentatxt"));
            String estado = request.getParameter("customRadio");
            String usu = Variables.usuario1;
            int length = descrip.length();
            if (length == 0 || cotiCompra == 0 || cotiVenta == 0) {
                request.getRequestDispatcher("mensaje.jsp").forward(request, response);
            } else {
                mon.setDesc_moneda(descrip);
                mon.setCoti_compra_actual_moneda(cotiCompra);
                mon.setCoti_venta_actual_moneda(cotiVenta);
                mon.setEstado_moneda(estado);
                mon.setNom_usu_mod(usu);
                r = monbd.registrarMoneda(mon);
                if (r == 0) {
                    request.getRequestDispatcher("registradoReferenciales.jsp").forward(request, response);
                } else {
                    request.getRequestDispatcher("errorGeneral.jsp").forward(request, response);
                }
            }
        }

        if (accion.equals("Buscar")) {
            String buscar = request.getParameter("buscartxt");
            int length = buscar.length();
            mon = mon.buscarMoneda(buscar);
            request.setAttribute("Moneda", mon);
            request.getRequestDispatcher("moneda.jsp").forward(request, response);
        }

        if (accion.equals("Moneda")) {
            String buscar = request.getParameter("buscartxt");
            int length = buscar.length();
            mon = mon.buscarMoneda(buscar);
            request.setAttribute("Moneda", mon);
            request.getRequestDispatcher("moneda.jsp").forward(request, response);
        }

        if (accion.equals("Modificar")) {
            String descrip = request.getParameter("descriptxt");
            int cotiCompra = Integer.valueOf(request.getParameter("cotiCompratxt"));
            int cotiVenta = Integer.valueOf(request.getParameter("cotiVentatxt"));
            String estado = request.getParameter("customRadio");
            String usu = Variables.usuario1;
            int id_mon = Integer.valueOf(request.getParameter("idtxt"));
            int length = descrip.length();
            if (length == 0 || cotiCompra == 0 || cotiVenta == 0) {
                request.getRequestDispatcher("mensaje.jsp").forward(request, response);
            } else {
                mon.setDesc_moneda(descrip);
                mon.setCoti_compra_actual_moneda(cotiCompra);
                mon.setCoti_venta_actual_moneda(cotiVenta);
                mon.setEstado_moneda(estado);
                mon.setNom_usu_mod(usu);
                mon.setId_moneda(id_mon);
                r = monbd.modificarMoneda(mon);
                if (r == 0) {
                    request.getRequestDispatcher("registradoReferenciales.jsp").forward(request, response);
                } else {
                    request.getRequestDispatcher("errorGeneral.jsp").forward(request, response);
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
