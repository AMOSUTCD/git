/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Contrato;
import model.Variables;

/**
 *
 * @author Alvaro
 */
@WebServlet(name = "contrato", urlPatterns = {"/contrato"})
public class contrato extends HttpServlet {

    Contrato cont = new Contrato();
    Contrato contbd = new Contrato();
    int r;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String accion = request.getParameter("accion");

        if (accion.equals("Guardar")) {

            String cliente_cont = request.getParameter("clientetxt");
            String fiscal_cont = request.getParameter("fiscaltxt");
            String codigo_cont = request.getParameter("codigocontratotxt");
            String validez_cont = request.getParameter("valideztxt");
            String fecha_inicio = request.getParameter("fechainiciotxt");
            String fecha_fin = request.getParameter("fechafin");
            String tipo_cont = request.getParameter("tipocontratotxt");
            String estado_cont = request.getParameter("customRadio");
            String usu = Variables.usuario1;
            int clienteid = Integer.valueOf(request.getParameter("drop_clie"));

            int length = cliente_cont.length();
            int length2 = fiscal_cont.length();
            int length3 = codigo_cont.length();
            int length4 = validez_cont.length();
            int length5 = fecha_inicio.length();
            if (length == 0 || length2 == 0 || length3 == 0 || length4 == 0 || length5 == 0) {
                request.getRequestDispatcher("mensaje.jsp").forward(request, response);
            } else {

                cont.setCliente_contrato(cliente_cont);
                cont.setFiscal_contrato(fiscal_cont);
                cont.setCodigo_contrato(codigo_cont);
                cont.setValidez_contrato(validez_cont);
                cont.setFecha_inicio(fecha_inicio);
                cont.setFecha_fin(fecha_fin);
                cont.setTipo_contrato(tipo_cont);
                cont.setEstado_contrato(estado_cont);
                cont.setNom_usu(usu);
                cont.setId_cliente(clienteid);
                
                r = contbd.registrarContrato(cont);
                if (r == 0) {
                    request.getRequestDispatcher("registradoReferenciales.jsp").forward(request, response);
                } else {
                    request.getRequestDispatcher("errorGeneral.jsp").forward(request, response);
                }
            }
        }
        if (accion.equals("Buscar")) {
            String buscar = request.getParameter("buscartxt");
            int length = buscar.length();
            cont = cont.buscarContrato(buscar);
            request.setAttribute("Contrato", cont);
            request.getRequestDispatcher("contrato.jsp").forward(request, response);

        }
        if (accion.equals("Contrato")) {
            String buscar = request.getParameter("buscartxt");
            int length = buscar.length();
            cont = cont.buscarContrato(buscar);
            request.setAttribute("Contrato", cont);
            request.getRequestDispatcher("contrato.jsp").forward(request, response);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
