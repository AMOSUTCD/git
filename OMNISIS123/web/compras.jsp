<%-- 
    Document   : compras
    Created on : 15/08/2021, 05:55:17 PM
    Author     : Alvaro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Compras</title>
        <link rel="icon" href="img/favicon.ico">   
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <STYLE TYPE="text/css" MEDIA=screen>
            <!-- 
            html,body{
                background-image: url('http://getwallpapers.com/wallpaper/full/a/5/d/544750.jpg');
                background-size: cover;
                background-repeat: no-repeat;
                height: 100%;
                font-family: 'Numans', sans-serif;
                color: white;

                .custab{
                    border: 1px solid #ccc;
                    padding: 5px;
                    margin: 5% 0;
                    box-shadow: 3px 3px 2px #ccc;
                    transition: 0.5s;
                }
                .custab:hover{
                    box-shadow: 3px 3px 0px transparent;
                    transition: 0.5s;
                }
            }

            .margin{
                margin-top:-3px;    
                margin-bottom:0px;
            }

            .themeBtn {
                background: #ff5c00;
                color: #ffffff !important;
                display: inline-block;
                font-size: 15px;
                font-weight: 500;
                height: 30px;
                line-height: 0.8;
                padding: 10px 20px;
                text-transform: capitalize;
                border-radius: 1px;
                letter-spacing: 0.5px;
                border:0px !important;
                cursor:pointer;
                border-radius:100px;

            }
            a:hover{
                color: #ffffff;
                text-decoration:none;
            }
            .themeBtn:hover {
                background: rgb(255, 92, 0);
                color: #ffffff;
                box-shadow: 0 10px 25px -2px rgba(255, 92, 0, 0.6);
            }
            .themeBtn2 {
                background: #7600ff;
                color: #ffffff !important;
                display: inline-block;
                font-size: 15px;
                font-weight: 500;
                height: 50px;
                line-height: 0.8;
                padding: 18px 30px;
                text-transform: capitalize;
                border-radius: 1px;
                letter-spacing: 0.5px;
                border:0px !important;
                cursor:pointer;
                border-radius:100px;

            }
            .themeBtn2:hover {
                background: rgb(118, 0, 255);
                color: #ffffff;
                box-shadow: 0 10px 25px -2px rgba(118, 0, 255, 0.6);
            }
            .themeBtn3 {
                background: #0f8515;
                color: #ffffff !important;
                display: inline-block;
                font-size: 12px;
                font-weight: 500;
                height: 30px;
                line-height: 0.8;
                padding: 10px 20px;
                text-transform: capitalize;
                border-radius: 1px;
                letter-spacing: 0.5px;
                border:0px !important;
                cursor:pointer;
                border-radius:80px;

            }
            .themeBtn3:hover {
                background: rgb(255, 46, 77);
                color: #ffffff;
                box-shadow: 0 10px 25px -2px rgba(255, 46, 77, 0.6);
            }
            .themeBtn4 {
                background: #006eff;
                color: #ffffff !important;
                display: inline-block;
                font-size: 15px;
                font-weight: 500;
                height: 30px;
                line-height: 0.8;
                padding: 10px 20px;
                text-transform: fullwidth;
                border-radius: 1px;
                letter-spacing: 0.5px;
                border:0px !important;
                cursor:pointer;
                border-radius:100px;

            }
            .themeBtn4:hover {
                background: rgb(0, 110, 255);
                color: #ffffff;
                box-shadow: 0 10px 25px -2px rgba(0, 110, 255, 0.6);
            }


            .input-icons i {
                position: absolute;
            }

            .input-icons {
                width: 100%;
                margin-bottom: 10px;
            }

            .icon {
                padding: 10px;
                min-width: 40px;
            }



            --></style>
        <link rel="stylesheet" href="css/estilos.css">
    </head>
    <body>
        <%-- NUEVO MENÚ--%>
        <div>
            <%@page import="controller.pedidocompra"%>
            <%@page import="controller.aprobacioncompra"%>
            <%@page import="controller.ordencompra"%>
            

            <nav class="navbar navbar-dark navbar-expand-md navigation-clean-search">
                <ul class="nav navbar-nav">
                    <form action="pedidocompra" method="POST">
                        <input type="hidden" name="buscartxt" value=" "> 
                        <li class="dropdown">
                            <input type="submit" name="accion" target="_blank" class="btn btn-light action-button" value="Pedido" >
                        </li>
                    </form>
                    <form action="aprobaciondecompra" method="POST">
                        <input type="hidden" name="buscartxt" value=" "> 
                        <li class="dropdown">
                            <input type="submit" name="accion" target="_blank" class="btn btn-light action-button" value="Aprobacion">
                        </li>
                    </form>
                    <form action="ordencompra" method="POST">
                        <input type="hidden" name="buscartxt" value=" "> 
                        <li class="dropdown">
                            <input type="submit" name="accion" target="_blank" class="btn btn-light action-button" value="Orden">
                        </li>
                    </form>
                    <form action="registrarcompra.jsp" method="POST">
                        <input type="hidden" name="buscartxt" value=" "> 
                        <li class="dropdown">
                            <input type="submit" name="accion" target="_blank" class="btn btn-light action-button" value="Registrar compra">
                        </li>
                    </form>
                    <form action="consultaPrest.jsp" method="POST">
                        <input type="hidden" name="buscartxt" value=" "> 
                        <li class="dropdown">
                            <input type="submit" name="accion" target="_blank" class="btn btn-light action-button" value="Prueba">
                        </li>
                    </form>

                </ul>
                <form class="form-inline mr-auto" target="_self">

                </form><a class="btn btn-light action-button" role="button" href="Principal.jsp">Atrás</a>
            </nav>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
        <%--NUEVO MENÚ--%>
        <div class="brand_logo_container">
            <img src="https://omni.com.py/img/logo.png" width="300" height="300" class="brand_logo" alt="Logo">
        </div>
    </body>
</html>
